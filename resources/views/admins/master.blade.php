<!DOCTYPE html>
<html lang="en">

<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>


    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ] -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">


    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{asset('css/nifty.min.css')}}" rel="stylesheet">


    <!--Font Awesome [ OPTIONAL ]-->
    <link href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">


    <!--Animate.css [ OPTIONAL ]-->
    <link href="{{asset('plugins/animate-css/animate.min.css')}}" rel="stylesheet">


    <!--Morris.js [ OPTIONAL ]-->
    <link href="{{asset('plugins/morris-js/morris.min.css')}}" rel="stylesheet">


    <!--Switchery [ OPTIONAL ]-->
    <link href="{{asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet">


    <!--Bootstrap Select [ OPTIONAL ]-->
    <link href="{{asset('plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet">


    <!--Demo script [ DEMONSTRATION ]-->
    <link href="{{asset('css/demo/nifty-demo.min.css')}}" rel="stylesheet">




    <!--SCRIPT-->
    <!--=================================================-->

    <!--Page Load Progress Bar [ OPTIONAL ]-->
    <link href="{{asset('plugins/pace/pace.min.css')}}" rel="stylesheet">
    <script src="{{asset('plugins/pace/pace.min.js')}}"></script>
    @yield('css')

</head>

<body>
<div id="container" class="effect mainnav-lg">

    @include('admins.layouts.navbar')

    <div class="boxed">
        @yield('content')

        @include('admins.layouts.mainnav')

    </div>



   @include('admins.layouts.footer')

</div>
<!--===================================================-->
<!-- END OF CONTAINER -->





<!--JAVASCRIPT-->
<!--=================================================-->

<!--jQuery [ REQUIRED ]-->
<script src="{{asset('js/jquery-2.1.1.min.js')}}"></script>


<!--BootstrapJS [ RECOMMENDED ]-->
<script src="{{asset('js/bootstrap.min.js')}}"></script>


<!--Fast Click [ OPTIONAL ]-->
<script src="{{asset('plugins/fast-click/fastclick.min.js')}}"></script>


<!--Nifty Admin [ RECOMMENDED ]-->
<script src="{{asset('js/nifty.min.js')}}"></script>


<!--Morris.js [ OPTIONAL ]-->
<script src="{{asset('plugins/morris-js/morris.min.js')}}"></script>
<script src="{{asset('plugins/morris-js/raphael-js/raphael.min.js')}}"></script>


<!--Sparkline [ OPTIONAL ]-->
<script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>


<!--Skycons [ OPTIONAL ]-->
<script src="{{asset('plugins/skycons/skycons.min.js')}}"></script>


<!--Switchery [ OPTIONAL ]-->
<script src="{{asset('plugins/switchery/switchery.min.js')}}"></script>


<!--Bootstrap Select [ OPTIONAL ]-->
<script src="{{asset('plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>


<!--Demo script [ DEMONSTRATION ]-->
<script src="{{asset('js/demo/nifty-demo.min.js')}}"></script>


<!--Specify page [ SAMPLE ]-->
<script src="{{asset('js/demo/dashboard.js')}}"></script>
@yield('js')

</body>

</html>
